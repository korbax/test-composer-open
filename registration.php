<?php
/**
 * Copyright © Tiny Elephant, (https://tinyelephant.no/). All rights reserved.
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Korbax_TestOpen',
    __DIR__
);
